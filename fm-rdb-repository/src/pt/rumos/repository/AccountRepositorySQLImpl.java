package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Account;
import pt.rumos.model.Client;

public class AccountRepositorySQLImpl implements AccountRepository {

	@Override
	public Optional<Account> save(Account account) {
		account.setAccountNumber(getMaxAccount());
		while (findByAccountNumber(account.getAccountNumber()).isPresent()) {
			account.setAccountNumber(account.getAccountNumber() + 1);
		}
		
		String sql = "INSERT INTO account (id, account_number, owner_id, balance) "
				+ "VALUES (0, " 
				+ account.getAccountNumber() + ", "
				+ account.getOwner().getId() + ", "
				+ account.getBalance() + ");";

		Database.execute(sql);
		return findByAccountNumber(account.getAccountNumber());
	}

	@Override
	public Optional<Account> update(Account account) {
		String sql = "UPDATE account SET "
				+ "account_number = " + account.getAccountNumber() + ","
				+ "owner_id       = " + account.getOwner().getId() + ","
				+ "balance        = " + account.getBalance() + " "
				+ "WHERE id = " + account.getId() + ";";
		
		Database.execute(sql);
		return findById(account.getId());
	}

	@Override
	public List<Account> findAll() {
		String sql = "SELECT * FROM account;";
		
		ResultSet result = Database.executeQuery(sql);
		List<Account> resultList = extractList(result);
		
		for (Account r : resultList) {
			r.setSecondaryOwners(getAllSecondaryOwners(r));
		}
		
		return resultList;
	}

	@Override
	public Optional<Account> findById(Long id) {
		String sql = "SELECT * FROM account WHERE id = " + id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		Optional<Account> account = extractObject(result);
		if (account.isPresent()) {
			account.get().setSecondaryOwners(getAllSecondaryOwners(account.get()));
			return account;
		}
		else {
			return extractObject(result);
		}
	}
	
	@Override
	public Optional<Account> findByAccountNumber(Integer account_number) {
		String sql = "SELECT * FROM account WHERE account_number = " + account_number + ";";
		
		ResultSet result = Database.executeQuery(sql);
		Optional<Account> account = extractObject(result);
		if (account.isPresent()) {
			account.get().setSecondaryOwners(getAllSecondaryOwners(account.get()));
			return account;
		}
		else {
			return extractObject(result);
		}
	}

	@Override
	public void deleteById(Long id) {
		String sql = "DELETE FROM account WHERE id = " + id + ";";
		
		Database.execute(sql);
	}
	
	@Override
	public void deleteSecondaryOwners(Long account_id) {
		String sql = "DELETE FROM secondary_owners WHERE account_id = " + account_id + ";";
		
		Database.execute(sql);
	}
	
	@Override
	public Optional<Account> addSecondaryOwner(Account account, Long client_id) {
		String sql = "INSERT INTO secondary_owners (account_id, client_id ) "
				+ "VALUES ("
				+ account.getId() + ", "
				+ client_id + ");";
		
		Database.execute(sql);
		return findById(account.getId());
	}
	

	@Override
	public Optional<Account> deleteSecondaryOwner(Account account, Long client_id) {
		String sql = "DELETE FROM secondary_owners WHERE account_id = " + account.getId() + " AND client_id = " + client_id;
		
		Database.execute(sql);
		return findById(account.getId());
	}
	
	private Integer getMaxAccount() {
		String sql = "SELECT MAX(account_number) FROM account";
		
		Integer max = 1000000;
		try {
			ResultSet result = Database.executeQuery(sql);
			Integer resultMax = result.getInt(1);
			if (resultMax > max) {
				max = resultMax;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return max+1;
	}
	
	private List<Client> getAllSecondaryOwners(Account account){
		String sql = "SELECT client_id FROM secondary_owners WHERE account_id = " + account.getId() + ";";
		
		ResultSet resultSec = Database.executeQuery(sql);
		
		return extractListSecondaryOwners(resultSec);
	}
	
	private List<Account> extractList(ResultSet result) {
		try {
			List<Account> accounts = new ArrayList<Account>();
			while (result.next()) {
				Account account = buildObject(result);
				accounts.add(account);
			}
			
			return accounts;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	private Optional<Account> extractObject(ResultSet result) {
		try {
			if (result.next()) {
				Account account = buildObject(result);
				return Optional.of(account);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Optional.empty();
	}
	
	private Account buildObject(ResultSet result) throws SQLException {
		Account account = new Account();
		
		account.setId(result.getLong(1));
		account.setAccountNumber(result.getInt(2));
		Client client = new Client();
		client.setId(result.getLong(3));
		account.setOwner(client);
		account.setBalance(result.getDouble(4));
		
		return account;
	}
	
	private List<Client> extractListSecondaryOwners(ResultSet result) {
		try {
			List<Client> secondaryOwners = new ArrayList<Client>();
			while (result.next()) {
				Client client = new Client();
				client.setId(result.getLong(1));
				secondaryOwners.add(client);
			}
			
			return secondaryOwners;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

}
