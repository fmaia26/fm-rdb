package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Account;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;

public class CreditCardRepositorySQLImpl implements CreditCardRepository {

	@Override
	public Optional<CreditCard> save(CreditCard credit_card) {
		credit_card.setUniqueCode(getMaxUniqueCode());
		while (findByUniqCode(credit_card.getUniqueCode()).isPresent()) {
			credit_card.setUniqueCode(credit_card.getUniqueCode() + 1);
		}
		
		String sql = "INSERT INTO credit_card (id, client_id, account_id, unique_code, pin, last_move, value_day, must_change_pass) "
				+ "VALUES (0, " 
				+ credit_card.getClient().getId() + ", "
				+ credit_card.getAccount().getId() + ", "
				+ credit_card.getUniqueCode() + ", "
				+ credit_card.getPin() + ", "
				+ "'" + credit_card.getLastMove() + "', "
				+ credit_card.getValue() + ", "
				+ credit_card.getMustChangePass() + ");";
		
		Database.execute(sql);
		return findByClient(credit_card.getClient().getId());
	}

	@Override
	public Optional<CreditCard> update(CreditCard credit_card) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CreditCard> findAll() {
		String sql = "SELECT * FROM credit_card;";
		
		ResultSet result = Database.executeQuery(sql);		
		return extractList(result);
	}

	@Override
	public Optional<CreditCard> findById(Long id) {
		String sql = "SELECT * FROM credit_card WHERE id = " + id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}
	

	@Override
	public Optional<CreditCard> findByClient(Long client_id) {
		String sql = "SELECT * FROM credit_card WHERE client_�d = " + client_id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}

	@Override
	public void deleteById(Long id) {
		String sql = "DELETE FROM credit_card WHERE id = " + id + ";";
		
		Database.execute(sql);
	}
	
	private Integer getMaxUniqueCode() {
		String sql = "SELECT MAX(unique_code) FROM credit_card";
		
		Integer max = 1000;
		try {
			Integer result = Database.executeQuery(sql).getInt(1);
			if (result > max) {
				max = result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return max+1;
	}
	
	private Optional<CreditCard> findByUniqCode(Integer unique_code) {
		String sql = "SELECT * FROM credit_card WHERE unique_code = " + unique_code + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}
	
	private List<CreditCard> extractList(ResultSet result) {
		try {
			List<CreditCard> credit_cards = new ArrayList<CreditCard>();
			while (result.next()) {
				CreditCard credit_card = buildObject(result);
				credit_cards.add(credit_card);
			}
			
			return credit_cards;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	private Optional<CreditCard> extractObject(ResultSet result) {
		try {
			if (result.next()) {
				CreditCard credit_card = buildObject(result);
				return Optional.of(credit_card);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Optional.empty();
	}
	
	private CreditCard buildObject(ResultSet result) throws SQLException {
		CreditCard credit_card = new CreditCard();
		
		credit_card.setId(result.getLong(1));
		Client client = new Client();
		client.setId(result.getLong(2));
		credit_card.setClient(client);
		Account account = new Account();
		account.setAccountNumber(result.getInt(3));
		credit_card.setUniqueCode(result.getInt(4));
		credit_card.setPin(result.getInt(5));
		credit_card.setLastMove(result.getDate(6).toLocalDate());
		credit_card.setValue(result.getDouble(7));
		credit_card.setMustChangePass(result.getBoolean(8));
		
		return credit_card;
	}

}
