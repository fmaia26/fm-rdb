package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Client;

public class ClientRepositorySQLImpl implements ClientRepository {

	@Override
	public Optional<Client> save(Client client) {
		String sql = "INSERT INTO client (id, name, nif, date_of_birth, occupation, phone, cell_phone, email, password) "
				+ "VALUES (0, "
				+ "'" + client.getName() + "', "
				+ "'" + client.getNif() + "', "
				+ "'" + client.getDateOfBirth() + "', "
				+ "'" + client.getOccupation() + "', "
				+ "'" + client.getPhone() + "', "
				+ "'" + client.getCellPhone() + "', "
				+ "'" + client.getEmail() + "', "
				+ "'" + client.getPassword() + "');";

		Database.execute(sql);
		return findByNif(client.getNif());
	}

	@Override
	public Optional<Client> update(Client client) {
		String sql = "UPDATE client SET "
				+ "name          = '" + client.getName() + "',"
				+ "nif           = '" + client.getNif() + "',"
				+ "date_of_birth = '" + client.getDateOfBirth() + "',"
				+ "occupation    = '" + client.getOccupation() + "',"
				+ "phone         = '" + client.getPhone() + "',"
				+ "cell_phone    = '" + client.getCellPhone() + "',"
				+ "email         = '" + client.getEmail() + "',"
				+ "password      = '" + client.getPassword() + "' "
				+ "WHERE id      = " + client.getId() + ";";

		Database.execute(sql);
		return findById(client.getId());
	}

	@Override
	public List<Client> findAll() {
		String sql = "SELECT * FROM client;";
		
		ResultSet result = Database.executeQuery(sql);
		return extractList(result);
	}

	@Override
	public Optional<Client> findById(Long id) {
		String sql = "SELECT * FROM client WHERE id = " + id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}

	@Override
	public Optional<Client> findByNif(String nif) {
		String sql = "SELECT * FROM client WHERE nif = '" + nif + "';";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}

	@Override
	public void deleteById(Long id) {
		String sql = "DELETE FROM client WHERE id = " + id + ";";
		
		Database.execute(sql);
	}

	private List<Client> extractList(ResultSet result) {
		try {
			List<Client> clients = new ArrayList<Client>();
			while (result.next()) {
				Client client = buildObject(result);
				clients.add(client);
			}
			
			return clients;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	private Optional<Client> extractObject(ResultSet result) {
		try {
			if (result.next()) {
				Client client = buildObject(result);
				return Optional.of(client);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Optional.empty();
	}
	
	private Client buildObject(ResultSet result) throws SQLException {
		Client client = new Client();
		
		client.setId(result.getLong(1));
		client.setName(result.getString(2));
		client.setNif(result.getString(3));
		client.setDateOfBirth(result.getDate(4).toLocalDate());
		client.setOccupation(result.getString(5));
		client.setPhone(result.getString(6));
		client.setCellPhone(result.getString(7));
		client.setEmail(result.getString(8));
		client.setPassword(result.getString(9));
		
		return client;
	}

}
