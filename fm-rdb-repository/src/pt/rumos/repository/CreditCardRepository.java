package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.CreditCard;

public interface CreditCardRepository {

	Optional<CreditCard> save(CreditCard credit_card);
	
	Optional<CreditCard> update(CreditCard credit_card);
	
	List<CreditCard> findAll();
	
	Optional<CreditCard> findById(Long id);
	
	Optional<CreditCard> findByClient(Long client_id);
	
	void deleteById(Long id);
	
}
