package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.DebitCard;

public interface DebitCardRepository {

	Optional<DebitCard> save(DebitCard debit_card);
	
	Optional<DebitCard> update(DebitCard debit_card);
	
	List<DebitCard> findAll();
	
	Optional<DebitCard> findById(Long id);
	
	Optional<DebitCard> findByClient(Long client_id);
	
	void deleteById(Long id);
	
}
