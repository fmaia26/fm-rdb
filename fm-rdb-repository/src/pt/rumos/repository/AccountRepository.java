package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Account;

public interface AccountRepository {

	Optional<Account> save(Account account);
	
	Optional<Account> update(Account account);
	
	List<Account> findAll();
	
	Optional<Account> findById(Long id);
	
	Optional<Account> findByAccountNumber(Integer account_number);
	
	void deleteById(Long id);
	
	void deleteSecondaryOwners(Long account_id);
	
	Optional<Account> addSecondaryOwner(Account account, Long Client_id);
	
	Optional<Account> deleteSecondaryOwner(Account account, Long Client_id);
	
}
