package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.database.Database;
import pt.rumos.model.Account;
import pt.rumos.model.Client;
import pt.rumos.model.DebitCard;

public class DebitCardRepositorySQLImpl implements DebitCardRepository {

	@Override
	public Optional<DebitCard> save(DebitCard debit_card) {
		debit_card.setUniqueCode(getMaxUniqueCode());
		while (findByUniqCode(debit_card.getUniqueCode()).isPresent()) {
			debit_card.setUniqueCode(debit_card.getUniqueCode() + 1);
		}
		
		String sql = "INSERT INTO debit_card (id, client_id, account_id, unique_code, pin, last_move, value_day, must_change_pass) "
				+ "VALUES (0, " 
				+ debit_card.getClient().getId() + ", "
				+ debit_card.getAccount().getId() + ", "
				+ debit_card.getUniqueCode() + ", "
				+ debit_card.getPin() + ", "
				+ "'" + debit_card.getLastMove() + "', "
				+ debit_card.getValue() + ", "
				+ debit_card.getMustChangePass() + ");";
		
		Database.execute(sql);
		return findByClient(debit_card.getClient().getId());
	}

	@Override
	public Optional<DebitCard> update(DebitCard debit_card) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DebitCard> findAll() {
		String sql = "SELECT * FROM debit_card;";
		
		ResultSet result = Database.executeQuery(sql);		
		return extractList(result);
	}

	@Override
	public Optional<DebitCard> findById(Long id) {
		String sql = "SELECT * FROM debit_card WHERE id = " + id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}
	

	@Override
	public Optional<DebitCard> findByClient(Long client_id) {
		String sql = "SELECT * FROM debit_card WHERE client_�d = " + client_id + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}

	@Override
	public void deleteById(Long id) {
		String sql = "DELETE FROM debit_cards WHERE id = " + id + ";";
		
		Database.execute(sql);
	}
	
	private Integer getMaxUniqueCode() {
		String sql = "SELECT MAX(unique_code) FROM debit_card";
		
		Integer max = 1000;
		try {
			Integer result = Database.executeQuery(sql).getInt(1);
			if (result > max) {
				max = result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return max+1;
	}
	
	private Optional<DebitCard> findByUniqCode(Integer unique_code) {
		String sql = "SELECT * FROM debit_card WHERE unique_code = " + unique_code + ";";
		
		ResultSet result = Database.executeQuery(sql);
		return extractObject(result);
	}
	
	private List<DebitCard> extractList(ResultSet result) {
		try {
			List<DebitCard> debit_cards = new ArrayList<DebitCard>();
			while (result.next()) {
				DebitCard debit_card = buildObject(result);
				debit_cards.add(debit_card);
			}
			
			return debit_cards;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}

	private Optional<DebitCard> extractObject(ResultSet result) {
		try {
			if (result.next()) {
				DebitCard debit_card = buildObject(result);
				return Optional.of(debit_card);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Optional.empty();
	}
	
	private DebitCard buildObject(ResultSet result) throws SQLException {
		DebitCard debit_card = new DebitCard();
		
		debit_card.setId(result.getLong(1));
		Client client = new Client();
		client.setId(result.getLong(2));
		debit_card.setClient(client);
		Account account = new Account();
		account.setAccountNumber(result.getInt(3));
		debit_card.setUniqueCode(result.getInt(4));
		debit_card.setPin(result.getInt(5));
		debit_card.setLastMove(result.getDate(6).toLocalDate());
		debit_card.setValue(result.getDouble(7));
		debit_card.setMustChangePass(result.getBoolean(8));
		
		return debit_card;
	}

}
