package pt.rumos.service;

import java.util.List;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Client;

public interface ClientService {

	Client save(Client client) throws RuleValidationException;
	
	Client update(Client client) throws ResourceNotFoundException, InformationNotUpdatableException;
	
	List<Client> getAll();
	
	Client getById(Long id) throws ResourceNotFoundException;
	
	Client getByNif(String nif) throws ResourceNotFoundException;
	
	Client delete(Long id) throws ResourceNotFoundException;
	
}
