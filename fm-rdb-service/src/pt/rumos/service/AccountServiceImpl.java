package pt.rumos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Account;
import pt.rumos.model.Client;
import pt.rumos.repository.AccountRepository;
import pt.rumos.repository.AccountRepositorySQLImpl;

public class AccountServiceImpl implements AccountService {

	private AccountRepository accountRepository;
	
	public AccountServiceImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	public AccountServiceImpl() {
		this.accountRepository = new AccountRepositorySQLImpl();
	}
	
	@Override
	public Account save(Account account) throws RuleValidationException {
		
		LocalDate legalDate = LocalDate.now().minusYears(18);
		if (account.getOwner().getDateOfBirth().compareTo(legalDate) > 0) {
			throw new RuleValidationException("Owner of account must be at least 18 years old");
		}
		
		if (account.getBalance() < 50) {
			throw new RuleValidationException("Minimum initial balance must be at least 50 �");
		}
		
		Optional<Account> savedAccount = accountRepository.save(account);
		
		if (savedAccount.isPresent()) {
			return savedAccount.get();
		} else {
			throw new RuntimeException("Account was not saved");
		}
	}

	@Override
	public Account update(Account account) throws ResourceNotFoundException, InformationNotUpdatableException {
		
		return null;
	}

	@Override
	public List<Account> getAll() {
		List<Account> realAccounts = new ArrayList<Account>();
		List<Account> accounts = accountRepository.findAll();
		for (int i = 0; i < accounts.size(); i++) {
			if (accounts.get(i) != null) {
				realAccounts.add(accounts.get(i));
			}
		}
		return realAccounts;
	}

	@Override
	public Account getById(Long id) throws ResourceNotFoundException {
		Optional<Account> selectedAccount = accountRepository.findById(id);
		
		if (selectedAccount.isPresent()) {
			return selectedAccount.get();
		} else {
			throw new ResourceNotFoundException("Account with ID " + id + " not found.");
		}
	}

	@Override
	public Account getByAccountNumber(Integer account_number) throws ResourceNotFoundException {
		Optional<Account> selectedAccount = accountRepository.findByAccountNumber(account_number);
		
		if (selectedAccount.isPresent()) {
			return selectedAccount.get();
		} else {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
	}

	@Override
	public Account delete(Integer account_number) throws ResourceNotFoundException {
		Optional<Account> selectedAccount = accountRepository.findByAccountNumber(account_number);
		if (selectedAccount.isPresent()) {
			return selectedAccount.get();
		} else {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + "doesnt exist.");
		}
	}
	
	@Override
	public Account addSecondaryOwner(Integer account_number, Long client_id) throws ResourceNotFoundException, RuleValidationException {
		Optional<Account> selectedAccount = accountRepository.findByAccountNumber(account_number);
		
		if (selectedAccount.isEmpty()) {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
		
		Account account = selectedAccount.get();
		if (account.getSecondaryOwners().size() >= 4) {
			throw new RuleValidationException("Accounts cannot have more than 4 secondary owners");
		}
		
		if (acountHasClient(account_number, client_id)) {
			throw new RuleValidationException("Client ID " + client_id + " already is owner of Account " + account_number);
		}
		
		selectedAccount = accountRepository.addSecondaryOwner(account, client_id);
		
		if (selectedAccount.isPresent()) {
			return selectedAccount.get();
		} else {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
	}

	@Override
	public Account removeSecondaryOwner(Integer account_number, Long client_id) throws ResourceNotFoundException {
		Optional<Account> selectedAccount = accountRepository.findByAccountNumber(account_number);
		
		if (selectedAccount.isEmpty()) {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
		
		Account account = selectedAccount.get();
		if (account.getSecondaryOwners().size() == 0) {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " doesnt have secondary owners");
		}
		
		boolean clientExist = false;
		for (Client c : account.getSecondaryOwners()) {
			if (c.getId() == client_id) {
				clientExist = true;
				break;
			}
		}
		
		if (!clientExist) {
			throw new ResourceNotFoundException("Client with ID " + client_id + " isnt secondary owner of Account " + account_number);
		}
		
		selectedAccount = accountRepository.deleteSecondaryOwner(account, client_id);
		
		if (selectedAccount.isPresent()) {
			return selectedAccount.get();
		} else {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
	}

	@Override
	public Boolean acountHasClient(Integer account_number, Long client_id) throws ResourceNotFoundException {
		Optional<Account> selectedAccount = accountRepository.findByAccountNumber(account_number);
		
		if (selectedAccount.isEmpty()) {
			throw new ResourceNotFoundException("Account with Account Number " + account_number + " not found.");
		}
		
		Account account = selectedAccount.get();
		
		if (account.getOwner().getId() == client_id) {
			return true;
		}
		
		Boolean isSecondary = false;
		for (Client c : account.getSecondaryOwners()) {
			if (c.getId() == client_id) {
				isSecondary = true;
				break;
			}
		}
		
		return isSecondary;
	}
	
	

}
