package pt.rumos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;
import pt.rumos.repository.CreditCardRepository;
import pt.rumos.repository.CreditCardRepositorySQLImpl;

public class CreditCardServiceImpl implements CreditCardService {

	private CreditCardRepository creditCardRepository;
	
	public CreditCardServiceImpl(CreditCardRepository creditCardRepository) {
		this.creditCardRepository = creditCardRepository;
	}
	
	public CreditCardServiceImpl() {
		this.creditCardRepository = new CreditCardRepositorySQLImpl();
	}
	
	@Override
	public CreditCard save(CreditCard credit_card) throws RuleValidationException {
		Optional<CreditCard> debit_cards = creditCardRepository.findByClient(credit_card.getClient().getId());
		
		if (debit_cards.isPresent()) {
			throw new RuleValidationException("Client already has a Credit Card.");
		}

		if(credit_card.getClient().getId() != credit_card.getAccount().getOwner().getId()) {
			Boolean isInAccount = false;
			for (Client c : credit_card.getAccount().getSecondaryOwners()) {
				if (c.getId() == credit_card.getClient().getId()) {
					isInAccount = true;
				}
			}
			
			if (!isInAccount) {
				throw new RuleValidationException("Client isn't owner of the account");
			}
		}
		
		credit_card.setLastMove(LocalDate.now());
		credit_card.setMustChangePass(true);
		
		int min = 1000;
		int max = 9999;
		credit_card.setPin((int)Math.floor(Math.random()*(max-min+1)+min));
		credit_card.setValue(0.0);
		
		creditCardRepository.save(credit_card);
		
		return null;
	}

	@Override
	public CreditCard update(CreditCard credit_card) throws ResourceNotFoundException, InformationNotUpdatableException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CreditCard> getAll() {
		List<CreditCard> realCreditCards = new ArrayList<CreditCard>();
		List<CreditCard> creditCards = creditCardRepository.findAll();
		for (int i = 0; i < creditCards.size(); i++) {
			if (creditCards.get(i) != null) {
				realCreditCards.add(creditCards.get(i));
			}
		}
		return realCreditCards;
	}

	@Override
	public CreditCard getById(Long id) throws ResourceNotFoundException {
		Optional<CreditCard> selectedDebitCard = creditCardRepository.findById(id);
		
		if (selectedDebitCard.isPresent()) {
			return selectedDebitCard.get();
		} else {
			throw new ResourceNotFoundException("Credit Card with ID: " + id + " not found.");
		}
	}
	
	@Override
	public CreditCard getByClient(Long client_id) throws ResourceNotFoundException {
		Optional<CreditCard> credit_card = creditCardRepository.findByClient(client_id);
		
		if (credit_card.isPresent()) {
			return credit_card.get();
		}
		else {
			throw new ResourceNotFoundException("Client doesn't have Credit Cards");
		}
	}

	@Override
	public CreditCard delete(Long id) throws ResourceNotFoundException {
		Optional<CreditCard> selectedCreditCard = creditCardRepository.findById(id);
		if (selectedCreditCard.isPresent()) {
			CreditCard credit_card = selectedCreditCard.get();
			creditCardRepository.deleteById(credit_card.getId());
			return credit_card;
		} else {
			throw new ResourceNotFoundException("Credit Card with ID " + id + "doesnt exist.");
		}
	}

}
