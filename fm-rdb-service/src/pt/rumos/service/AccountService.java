package pt.rumos.service;

import java.util.List;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Account;

public interface AccountService {

	Account save(Account account) throws RuleValidationException;
	
	Account update(Account account) throws ResourceNotFoundException, InformationNotUpdatableException;
	
	List<Account> getAll();
	
	Account getById(Long id) throws ResourceNotFoundException;
	
	Account getByAccountNumber(Integer account_number) throws ResourceNotFoundException;
	
	Account delete(Integer account_number) throws ResourceNotFoundException;
	
	Account addSecondaryOwner(Integer account_number, Long client_id) throws ResourceNotFoundException, RuleValidationException;
	
	Account removeSecondaryOwner(Integer account_number, Long client_id) throws ResourceNotFoundException;
	
	Boolean acountHasClient(Integer account, Long client_id) throws ResourceNotFoundException;
	
}
