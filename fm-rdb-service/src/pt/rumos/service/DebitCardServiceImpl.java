package pt.rumos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Client;
import pt.rumos.model.DebitCard;
import pt.rumos.repository.DebitCardRepository;
import pt.rumos.repository.DebitCardRepositorySQLImpl;

public class DebitCardServiceImpl implements DebitCardService {

	private DebitCardRepository debitCardRepository;
	
	public DebitCardServiceImpl(DebitCardRepository debitCardRepository) {
		this.debitCardRepository = debitCardRepository;
	}
	
	public DebitCardServiceImpl() {
		this.debitCardRepository = new DebitCardRepositorySQLImpl();
	}
	
	@Override
	public DebitCard save(DebitCard debit_card) throws RuleValidationException {
		Optional<DebitCard> debit_cards = debitCardRepository.findByClient(debit_card.getClient().getId());
		
		if (debit_cards.isPresent()) {
			throw new RuleValidationException("Client already has a Debit Card.");
		}

		if(debit_card.getClient().getId() != debit_card.getAccount().getOwner().getId()) {
			Boolean isInAccount = false;
			for (Client c : debit_card.getAccount().getSecondaryOwners()) {
				if (c.getId() == debit_card.getClient().getId()) {
					isInAccount = true;
				}
			}
			
			if (!isInAccount) {
				throw new RuleValidationException("Client isn't owner of the account");
			}
		}
		
		debit_card.setLastMove(LocalDate.now());
		debit_card.setMustChangePass(true);
		
		int min = 1000;
		int max = 9999;
		debit_card.setPin((int)Math.floor(Math.random()*(max-min+1)+min));
		debit_card.setValue(0.0);
		
		debitCardRepository.save(debit_card);
		
		return null;
	}

	@Override
	public DebitCard update(DebitCard debit_card) throws ResourceNotFoundException, InformationNotUpdatableException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DebitCard> getAll() {
		List<DebitCard> realDebitCards = new ArrayList<DebitCard>();
		List<DebitCard> debitCards = debitCardRepository.findAll();
		for (int i = 0; i < debitCards.size(); i++) {
			if (debitCards.get(i) != null) {
				realDebitCards.add(debitCards.get(i));
			}
		}
		return realDebitCards;
	}

	@Override
	public DebitCard getById(Long id) throws ResourceNotFoundException {
		Optional<DebitCard> selectedDebitCard = debitCardRepository.findById(id);
		
		if (selectedDebitCard.isPresent()) {
			return selectedDebitCard.get();
		} else {
			throw new ResourceNotFoundException("Debit Card with ID: " + id + " not found.");
		}
	}
	
	@Override
	public DebitCard getByClient(Long client_id) throws ResourceNotFoundException {
		Optional<DebitCard> debit_card = debitCardRepository.findByClient(client_id);
		
		if (debit_card.isPresent()) {
			return debit_card.get();
		}
		else {
			throw new ResourceNotFoundException("Client doesn't have Debit Cards");
		}
	}

	@Override
	public DebitCard delete(Long id) throws ResourceNotFoundException {
		Optional<DebitCard> selectedDebitCard = debitCardRepository.findById(id);
		if (selectedDebitCard.isPresent()) {
			DebitCard debit_card = selectedDebitCard.get();
			debitCardRepository.deleteById(debit_card.getId());
			return debit_card;
		} else {
			throw new ResourceNotFoundException("Debit Card with ID " + id + "doesnt exist.");
		}
	}

}
