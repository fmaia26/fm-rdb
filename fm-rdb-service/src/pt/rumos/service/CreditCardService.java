package pt.rumos.service;

import java.util.List;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.CreditCard;

public interface CreditCardService {

	CreditCard save(CreditCard debit_card) throws RuleValidationException;
	
	CreditCard update(CreditCard debit_card) throws ResourceNotFoundException, InformationNotUpdatableException;
	
	List<CreditCard> getAll();
	
	CreditCard getById(Long id) throws ResourceNotFoundException;
	
	CreditCard getByClient(Long client_id) throws ResourceNotFoundException;
	
	CreditCard delete(Long id) throws ResourceNotFoundException;
	
}
