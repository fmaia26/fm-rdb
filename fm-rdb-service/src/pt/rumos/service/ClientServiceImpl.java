package pt.rumos.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Client;
import pt.rumos.repository.ClientRepository;
import pt.rumos.repository.ClientRepositorySQLImpl;

public class ClientServiceImpl implements ClientService {
	
	private ClientRepository clientRepository;
	
	public ClientServiceImpl(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}
	
	public ClientServiceImpl() {
		this.clientRepository = new ClientRepositorySQLImpl();
	}

	@Override
	public Client save(Client client) throws RuleValidationException {
		Optional<Client> checkClient = clientRepository.findByNif(client.getNif());
		if (checkClient.isPresent()) {
			throw new RuleValidationException("Already exists one client with the NIF " + client.getNif());
		}
		
		Optional<Client> savedClient = clientRepository.save(client);
		
		if (savedClient.isPresent()) {
			return savedClient.get();
		} else {
			throw new RuntimeException("Client was not saved");
		}
	}

	@Override
	public Client update(Client client) throws ResourceNotFoundException, InformationNotUpdatableException {
		Optional<Client> selectedClient = clientRepository.findById(client.getId());
		
		if (selectedClient.isEmpty()) {
			throw new ResourceNotFoundException("Client with ID " + client.getId() + "doesnt exist.");
		}
		
		Client foundClient = selectedClient.get();
		
		if (client.getId().compareTo(foundClient.getId()) != 0) {
			throw new InformationNotUpdatableException("ID is not updatable");
		}
		if (client.getName().compareTo(foundClient.getName()) != 0) {
			throw new InformationNotUpdatableException("Name is not updatable");
		}
		if (client.getNif().compareTo(foundClient.getNif()) != 0) {
			throw new InformationNotUpdatableException("NIF is not updatable");
		}
		if (client.getDateOfBirth().compareTo(foundClient.getDateOfBirth()) != 0) {
			throw new InformationNotUpdatableException("Date of birth is not updatable");
		}
		
		Optional<Client> updatedClient = clientRepository.update(client);
		
		if (updatedClient.isPresent()) {
			return updatedClient.get();
		} else {
			throw new RuntimeException("Client was not updated");
		}
	}

	@Override
	public List<Client> getAll() {
		List<Client> realClients = new ArrayList<Client>();
		List<Client> clients = clientRepository.findAll();
		for (int i = 0; i < clients.size(); i++) {
			if (clients.get(i) != null) {
				realClients.add(clients.get(i));
			}
		}
		return realClients;
	}

	@Override
	public Client getById(Long id) throws ResourceNotFoundException {
		Optional<Client> selectedClient = clientRepository.findById(id);
		
		if (selectedClient.isPresent()) {
			return selectedClient.get();
		} else {
			throw new ResourceNotFoundException("Client with ID: " + id + " not found.");
		}
	}
	
	@Override
	public Client getByNif(String nif) throws ResourceNotFoundException {
		Optional<Client> selectedClient = clientRepository.findByNif(nif);
		
		if (selectedClient.isPresent()) {
			return selectedClient.get();
		} else {
			throw new ResourceNotFoundException("Client with NIF " + nif + " not found.");
		}
	}

	@Override
	public Client delete(Long id) throws ResourceNotFoundException {
		Optional<Client> selectedClient = clientRepository.findById(id);
		if (selectedClient.isPresent()) {
			Client client = selectedClient.get();
			clientRepository.deleteById(client.getId());
			return client;
		} else {
			throw new ResourceNotFoundException("Client with ID " + id + "doesnt exist.");
		}
	}

}
