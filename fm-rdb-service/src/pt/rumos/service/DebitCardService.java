package pt.rumos.service;

import java.util.List;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.DebitCard;

public interface DebitCardService {

	DebitCard save(DebitCard debit_card) throws RuleValidationException;
	
	DebitCard update(DebitCard debit_card) throws ResourceNotFoundException, InformationNotUpdatableException;
	
	List<DebitCard> getAll();
	
	DebitCard getById(Long id) throws ResourceNotFoundException;
	
	DebitCard getByClient(Long client_id) throws ResourceNotFoundException;
	
	DebitCard delete(Long id) throws ResourceNotFoundException;
	
}
