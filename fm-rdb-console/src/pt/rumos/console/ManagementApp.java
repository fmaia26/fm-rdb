package pt.rumos.console;

import java.util.Scanner;
import java.time.LocalDate;

import pt.rumos.exception.InformationNotUpdatableException;
import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.exception.RuleValidationException;
import pt.rumos.model.Account;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;
import pt.rumos.model.DebitCard;
import pt.rumos.service.AccountService;
import pt.rumos.service.AccountServiceImpl;
import pt.rumos.service.ClientService;
import pt.rumos.service.ClientServiceImpl;
import pt.rumos.service.CreditCardService;
import pt.rumos.service.CreditCardServiceImpl;
import pt.rumos.service.DebitCardService;
import pt.rumos.service.DebitCardServiceImpl;

public class ManagementApp {

	private Scanner scanner = new Scanner(System.in);
	
	private ClientService clientService = new ClientServiceImpl();
	private AccountService accountService = new AccountServiceImpl();
	private DebitCardService debitCardService = new DebitCardServiceImpl();
	private CreditCardService creditCardService = new CreditCardServiceImpl();
	
	public void run() {
		int option = 0;
		
		do {
			System.out.println("Select an Option:");
			System.out.println("0: Exit");
			System.out.println("1: Clients");
			System.out.println("2: Accounts");
			System.out.println("3: Bank Cards");
			
			option = scanner.nextInt();
			switch (option) {
			case 1:
				clientManagement();
				break;
			case 2:
				accountManagement();
				break;
			case 3:
				cardsManagement();
				break;
			}
		} while (option != 0);
		
		System.out.println("Thanks for using the Management Module");
	}
	
	private void clientManagement() {
		int option = 0;
		
		do {
			System.out.println();
			System.out.println("Select an Option:");
			System.out.println("0: Exit");
			System.out.println("1: Add Client");
			System.out.println("2: List All Clients");
			System.out.println("3: Show Client By NIF");
			System.out.println("4: Show Client By ID");
			System.out.println("5: Change Personal Information");
			System.out.println("6: Delete Client By ID");
			
			option = scanner.nextInt();
			
			switch (option) {
			case 1:
				createNewClient();
				break;
			case 2:
				showAllClients();
				break;
			case 3:
				showClientByNif();
				break;
			case 4:
				showClientById();
				break;
			case 5:
				updatePersonalInformation();
				break;
			case 6:
				deleteClient();
				break;
			}
		} while (option != 0);
	}
	
	private void createNewClient() {
		Client client = new Client();
		
		System.out.print("Please Insert Client Name: ");
		String name = scanner.next().strip();
		client.setName(name);
		
		System.out.print("Please Insert Client NIF: ");
		String nif = scanner.next().strip();
		client.setNif(nif);
		
		System.out.print("Please Insert Client Date Of Birth: ");
		String dateOfBirth = scanner.next().strip();
		client.setDateOfBirth(LocalDate.parse(dateOfBirth));
		
		System.out.print("Please Insert Client occupation: ");
		String occupation = scanner.next().strip();
		client.setOccupation(occupation);
		
		System.out.print("Please Insert Client Phone: ");
		String phone = scanner.next().strip();
		client.setPhone(phone);
		
		System.out.print("Please Insert Client Cell Phone: ");
		String cellPhone = scanner.next().strip();
		client.setCellPhone(cellPhone);
		
		System.out.print("Please Insert Client Email: ");
		String email = scanner.next().strip();
		client.setEmail(email);
		
		System.out.print("Please Insert Client Password: ");
		String password = scanner.next().strip();
		client.setPassword(password);
		
		System.out.println();
		
		try {
			Client resultClient = clientService.save(client);
			System.out.println("Client created. " + resultClient);
			
		} catch (RuntimeException | RuleValidationException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void showAllClients() {
		for (Client c : clientService.getAll()) {
			System.out.println(c);
		}
	}
	
	private void showClientByNif() {
		System.out.print("Please insert client NIF: ");
		String nif = scanner.next().strip();
		
		System.out.println();
		
		try {
			Client client = clientService.getByNif(nif);
			System.out.println(client);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void showClientById() {
		System.out.print("Please insert client ID: ");
		Long id = scanner.nextLong();
		
		System.out.println();
		
		try {
			Client client = clientService.getById(id);
			System.out.println(client);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void updatePersonalInformation() {
		System.out.print("Please insert client ID: ");
		Long id = scanner.nextLong();
		
		int option = 0;
		String occupation = new String();
		String phone = new String();
		String cellPhone = new String();
		String email = new String();
		String password = new String();
		
		do {
			System.out.println();
			System.out.println("Select the information to change:");
			System.out.println("0: Exit");
			System.out.println("1: Occupation");
			System.out.println("2: Phone");
			System.out.println("3: Cell Phone");
			System.out.println("4: Email");
			System.out.println("5: Password");
			
			option = scanner.nextInt();
			
			switch (option) {
			case 1:
				System.out.print("Occupation: ");
				occupation = scanner.next().strip();
				break;
			case 2:
				System.out.print("Phone: ");
				phone = scanner.next().strip();
				break;
			case 3:
				System.out.print("Cell Phone: ");
				cellPhone = scanner.next().strip();
				break;
			case 4:
				System.out.print("Email: ");
				email = scanner.next().strip();
				break;
			case 5:
				System.out.print("Password: ");
				password = scanner.next().strip();
				break;
			}
		} while (option != 0);
		
		System.out.println();
		
		try {
			Client client = clientService.getById(id);
			
			if (occupation.length() > 0) {
				client.setOccupation(occupation);
			}
			if (phone.length() > 0) {
				client.setPhone(phone);
			}
			if (cellPhone.length() > 0) {
				client.setCellPhone(cellPhone);
			}
			if (email.length() > 0) {
				client.setEmail(email);
			}
			if (password.length() > 0) {
				client.setPassword(password);
			}
			
			Client updatedClient = clientService.update(client);
			System.out.println("Client updated. " + updatedClient);
			
		} catch (ResourceNotFoundException | InformationNotUpdatableException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void deleteClient() {
		System.out.print("Please insert client ID: ");
		Long id = scanner.nextLong();
		
		System.out.println();
		
		try {
			Client deletedClient = clientService.delete(id);
			System.out.println("Client deleted. " + deletedClient);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void accountManagement() {
		int option = 0;
		
		do {
			System.out.println();
			System.out.println("Select an Option:");
			System.out.println("0: Exit");
			System.out.println("1: Add Account");
			System.out.println("2: List All Accounts");
			System.out.println("3: Show Account By Account Number");
			System.out.println("4: Add Secondary Owner");
			System.out.println("5: Remove Secondary Owner");
			System.out.println("6: Delete Account By Account Number");
			
			option = scanner.nextInt();
			
			switch (option) {
			case 1:
				createNewAccount();
				break;
			case 2:
				showAllAccounts();
				break;
			case 3:
				showAccountByAccountNumber();
				break;
			case 4:
				addSecondaryOwner();
				break;
			case 5:
				removeSecondaryOwner();
				break;
			case 6:
				deleteAccountByAccountNumber();
				break;
			}
		} while (option != 0);
	}
	
	private void createNewAccount() {
		Account account = new Account();
		
		System.out.println("Please insert owner ID:");
		for (Client c : clientService.getAll()) {
			System.out.println(c);
		}
		
		Long client_id = scanner.nextLong();
		Client owner;
		try {
			owner = clientService.getById(client_id);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		account.setOwner(owner);
		
		System.out.print("Please insert initial balance: ");
		Double balance = scanner.nextDouble();
		
		account.setBalance(balance);
		
		System.out.println();
		
		try {
			accountService.save(account);
			
		} catch (RuleValidationException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void showAllAccounts() {
		System.out.println();
		for (Account a : accountService.getAll()) {
			System.out.println(a);
		}
	}
	
	private void showAccountByAccountNumber() {
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		System.out.println();
		try {
			Account account = accountService.getByAccountNumber(account_number);
			System.out.println(account);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void addSecondaryOwner() {
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		System.out.println();
		
		System.out.println("Please insert owner ID:");
		for (Client c : clientService.getAll()) {
			System.out.println(c);
		}
		
		Long client_id = scanner.nextLong();
		
		System.out.println();
		
		try {
			Account account = accountService.addSecondaryOwner(account_number, client_id);
			System.out.println(account);
			
		} catch (ResourceNotFoundException | RuleValidationException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private void removeSecondaryOwner() {
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		System.out.print("Please insert owner ID: ");
		Long client_id = scanner.nextLong();
		
		System.out.println();
		
		try {
			Account account = accountService.removeSecondaryOwner(account_number, client_id);
			System.out.println(account);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void deleteAccountByAccountNumber() {
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		System.out.println();
		
		try {
			Account deletedAccount = accountService.delete(account_number);
			System.out.println("Account Deleted. " + deletedAccount);
			
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	private void cardsManagement() {
		int option = 0;
		
		do {
			System.out.println();
			System.out.println("Select an Option:");
			System.out.println("0: Exit");
			System.out.println("1: Create Debit Card");
			System.out.println("2: Create Credit Card");
			System.out.println("3: Show All Debit Cards");
			System.out.println("4: Show All Credit Cards");
			System.out.println("5: Show Cards By Client");
			System.out.println("6: Show Cards By Account");
			System.out.println("7: Remove Debit Card");
			System.out.println("8: Remove Credit Card");
			
			option = scanner.nextInt();
			
			switch (option) {
			case 1:
				createDebitCard();
				break;
			case 2:
				createCreditCard();
				break;
			case 3:
				showAllDebitCards();
				break;
			case 4:
				showAllCreditCards();
				break;
			case 5:
				showClientCards();
				break;
			case 6:
				showAccountCards();
				break;
			case 7:
				removeDebitCard();
				break;
			case 8:
				removeCreditCard();
				break;
			}
		} while (option != 0);
	}
	
	private void createDebitCard() {
		DebitCard debit_card = new DebitCard();
		
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		Account account;
		try {
			account = accountService.getByAccountNumber(account_number);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		debit_card.setAccount(account);
		
		System.out.print("Please insert client ID : ");
		Long client_id = scanner.nextLong();
		
		Client client;
		try {
			client = clientService.getById(client_id);
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		debit_card.setClient(client);
		
		System.out.println();
		
		try {
			debitCardService.save(debit_card);
			
		} catch (RuleValidationException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private void createCreditCard() {
		CreditCard credit_card = new CreditCard();
		
		System.out.print("Please insert account number: ");
		Integer account_number = scanner.nextInt();
		
		Account account;
		try {
			account = accountService.getByAccountNumber(account_number);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		credit_card.setAccount(account);
		
		System.out.print("Please insert client ID : ");
		Long client_id = scanner.nextLong();
		
		Client client;
		try {
			client = clientService.getById(client_id);
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}
		
		credit_card.setClient(client);
		
		System.out.println();
		
		try {
			creditCardService.save(credit_card);
			
		} catch (RuleValidationException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
	private void showAllDebitCards() {
		System.out.println();
		
		for (DebitCard dc : debitCardService.getAll()) {
			System.out.println(dc);
		}
	}
	
	private void showAllCreditCards() {
		System.out.println();
		
		for (CreditCard cc : creditCardService.getAll()) {
			System.out.println(cc);
		}
	}
	
	private void showClientCards() {
		System.out.print("Please insert client ID: ");
		Long client_id = scanner.nextLong(); 
		
		try {
			DebitCard debit_card = debitCardService.getByClient(client_id);
			System.out.println("DEBIT CARD: " + debit_card);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println();
		
		try {
			CreditCard credit_card = creditCardService.getByClient(client_id);
			System.out.println("CREDIT CARD: " + credit_card);
			
		} catch (ResourceNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void showAccountCards() {
		
	}
	
	private void removeDebitCard() {
		
	}
	
	private void removeCreditCard() {
		
	}
}
