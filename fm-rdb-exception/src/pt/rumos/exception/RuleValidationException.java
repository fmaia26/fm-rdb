package pt.rumos.exception;

public class RuleValidationException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public RuleValidationException() {
		
	}
	
	public RuleValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public RuleValidationException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public RuleValidationException(String message) {
		super(message);
	}
	
	public RuleValidationException(Throwable cause) {
		super(cause);
	}
}
