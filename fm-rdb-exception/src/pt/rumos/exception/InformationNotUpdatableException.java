package pt.rumos.exception;

public class InformationNotUpdatableException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InformationNotUpdatableException() {
		
	}
	
	public InformationNotUpdatableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public InformationNotUpdatableException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public InformationNotUpdatableException(String message) {
		super(message);
	}
	
	public InformationNotUpdatableException(Throwable cause) {
		super(cause);
	}

}
