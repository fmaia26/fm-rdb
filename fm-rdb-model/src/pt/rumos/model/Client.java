package pt.rumos.model;

import java.time.LocalDate;

public class Client {

	private Long id;
	private String name;
	private String nif;
	private LocalDate dateOfBirth;
	private String occupation;
	private String phone;
	private String cellPhone;
	private String email;
	private String password;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", nif=" + nif + ", dateOfBirth=" + dateOfBirth + ", occupation=" + occupation
				+ ", phone=" + phone + ", cellPhone=" + cellPhone + ", email=" + email + ", password=" + password + "]";
	}
}
