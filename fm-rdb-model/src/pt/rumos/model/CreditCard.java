package pt.rumos.model;

import java.time.LocalDate;

public class CreditCard {

	private Long id;
	private Client client;
	private Account account;
	private Integer uniqueCode;
	private Integer pin;
	private LocalDate lastMove;
	private Double value;
	private Boolean mustChangePass;
	private Double plafond;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Integer getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(Integer uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	
	public Integer getPin() {
		return pin;
	}
	public void setPin(Integer pin) {
		this.pin = pin;
	}
	
	public LocalDate getLastMove() {
		return lastMove;
	}
	public void setLastMove(LocalDate lastMove) {
		this.lastMove = lastMove;
	}
	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
	public Boolean getMustChangePass() {
		return mustChangePass;
	}
	public void setMustChangePass(Boolean mustChangePass) {
		this.mustChangePass = mustChangePass;
	}
	
	public Double getPlafond() {
		return plafond;
	}
	public void setPlafond(Double plafond) {
		this.plafond = plafond;
	}
	
	@Override
	public String toString() {
		return "CreditCard [id=" + id + ", client=" + client + ", account=" + account + ", uniqueCode=" + uniqueCode
				+ ", pin=" + pin + ", lastMove=" + lastMove + ", value=" + value + ", mustChangePass=" + mustChangePass
				+ ", plafond=" + plafond + "]";
	}
	
}
