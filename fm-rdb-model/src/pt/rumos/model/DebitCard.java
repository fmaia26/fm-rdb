package pt.rumos.model;

import java.time.LocalDate;

public class DebitCard {

	private Long id;
	private Client client;
	private Account account;
	private Integer uniqueCode;
	private Integer pin;
	private LocalDate lastMove;
	private Double valueDay;
	private Boolean mustChangePass;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	
	public Integer getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(Integer uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	
	public Integer getPin() {
		return pin;
	}
	public void setPin(Integer pin) {
		this.pin = pin;
	}
	
	public LocalDate getLastMove() {
		return lastMove;
	}
	public void setLastMove(LocalDate lastMove) {
		this.lastMove = lastMove;
	}
	
	public Double getValue() {
		return valueDay;
	}
	public void setValue(Double value) {
		this.valueDay = value;
	}
	
	public Boolean getMustChangePass() {
		return mustChangePass;
	}
	public void setMustChangePass(Boolean mustChangePass) {
		this.mustChangePass = mustChangePass;
	}
	
	@Override
	public String toString() {
		return "DebitCard [id=" + id + ", client=" + client + ", account=" + account + ", uniqueCode=" + uniqueCode
				+ ", PIN=" + pin + ", lastMove=" + lastMove + ", valueDay=" + valueDay + ", mustChangePass=" + mustChangePass
				+ "]";
	}
	
}
