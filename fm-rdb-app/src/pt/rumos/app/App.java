package pt.rumos.app;

import java.util.Scanner;

import pt.rumos.console.ManagementApp;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		ManagementApp managementApp = new ManagementApp();
		
		int option = 0;
		do {
			System.out.println("Select an option:");
			System.out.println("0: Exit");
			System.out.println("1: Management App");
			System.out.println("2: ATM App - CONSOLE");
			
			option = scanner.nextInt();
			switch(option) {
			case 1:
				managementApp.run();
				break;
			case 2:
				
				break;
			}
		} while (option != 0);
		
		System.out.println("Thanks for using RumosDigitalBank");
		scanner.close();
	}

}
